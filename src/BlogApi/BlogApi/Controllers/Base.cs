﻿using Microsoft.AspNetCore.Mvc;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;

namespace BlogApi.Controllers
{
    public class Base : ControllerBase
    {
        public Base()
        {

        }

        public string ToJson<Type>(Type objList)
        {
            DataContractJsonSerializer serialize = new DataContractJsonSerializer(typeof(Type));
            string output = string.Empty;
            using (MemoryStream ms = new MemoryStream())
            {
                serialize.WriteObject(ms, objList);
                output = Encoding.UTF8.GetString(ms.ToArray());
            }

            return output;
        }
    }
}
