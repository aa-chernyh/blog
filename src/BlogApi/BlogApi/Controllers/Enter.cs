﻿using BlogApi.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using BlogApi.Utilities;

namespace BlogApi.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class Enter : Base
    {
        private List<Models.Section> Sections { get; set; }
        private List<Models.Theme> Themes { get; set; }

        public Enter() {
            Models.Section section = new Models.Section();
            Models.Theme theme = new Models.Theme();

            Sections = section.GetList();
            Themes = theme.GetList();
        }

        [HttpGet("GetStruct")]
        public string GetStruct()
        {
            foreach (Section sect in Sections)
            {
                sect.Themes = Themes.Where(t => t.SectionId == sect.Id).ToList();
            }

            EnterResultStruct result = new EnterResultStruct
            {
                Status = 1,
                Data = Sections,
                Description = ResultDescription.ok
            };

            return ToJson(result);
        }

        [HttpGet("CreateSection")]
        public string CreateSection(int? userId, string name)
        {
            EnterResultStruct result = new EnterResultStruct();
            if (userId == null || name == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }

            Models.Section section = new Models.Section();
            bool queryResult = section.Create((int)userId, name);

            if (queryResult)
            {
                result.Status = 1;
                result.Description = ResultDescription.ok;
            }
            else
            {
                result.Status = 0;
                result.Description = ResultDescription.fewRights + " или " + ResultDescription.sectionExists;
            }

            return ToJson(result);
        }

        [HttpGet("CreateTheme")]
        public string CreateTheme(int? userId, int? sectionId, string name)
        {
            EnterResultStruct result = new EnterResultStruct();
            if (userId == null || sectionId == null || name == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }

            Models.Theme theme = new Models.Theme();
            bool queryResult = theme.Create((int)userId, (int)sectionId, name);

            if (queryResult)
            {
                result.Status = 1;
                result.Description = ResultDescription.ok;
            }
            else
            {
                result.Status = 0;
                result.Description = ResultDescription.fewRights;
            }

            return ToJson(result);
        }

        [HttpGet("HideTheme")]
        public string HideTheme(int? userId, int? themeId)
        {
            EnterResultStruct result = new EnterResultStruct();
            if (userId == null || themeId == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }

            Models.Theme theme = new Models.Theme();
            bool queryResult = theme.Hide((int)userId, (int)themeId);

            if (queryResult)
            {
                result.Status = 1;
                result.Description = ResultDescription.ok;
            }
            else
            {
                result.Status = 0;
                result.Description = ResultDescription.fewRights;
            }

            return ToJson(result);
        }
    }
}
