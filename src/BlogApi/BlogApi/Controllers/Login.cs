﻿using BlogApi.Enums;
using BlogApi.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BlogApi.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class Login : Base
    {
        [HttpGet("Go")]
        public string Go(string email, string password)
        {
            LoginResultStruct result = new LoginResultStruct();

            if (email == null || password == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }
            else
            {
                Models.User user = new Models.User(email);
                if (user.Password != password)
                {
                    result.Status = 0;
                    result.Description = ResultDescription.passwordDidNotMatch;
                }
                else
                {
                    result.Status = 1;
                    result.Description = ResultDescription.ok;
                    result.Data = user;
                }
            }

            return ToJson(result);
        }

        [HttpGet("Registration")]
        public string Registration(string name, string email, string password, string passwordConfirm)
        {
            LoginResultStruct result = new LoginResultStruct();

            if (name == null || email == null || password == null || passwordConfirm == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }
            else
            {
                if (password != passwordConfirm)
                {
                    result.Status = 0;
                    result.Description = ResultDescription.passwordsDidNotMatch;
                }
                else
                {
                    Models.User user = new Models.User();
                    if (user.Registration(name, email, password, UserPrivilege.User))
                    {
                        result.Status = 1;
                        result.Description = ResultDescription.ok;
                        result.Data = user;
                    }
                    else
                    {
                        result.Status = 0;
                        result.Description = ResultDescription.emailExists;
                    }
                }
            }

            return ToJson(result);
        }
    }
}
