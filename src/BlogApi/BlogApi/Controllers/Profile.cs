﻿using Microsoft.AspNetCore.Mvc;
using BlogApi.Utilities;

namespace BlogApi.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class Profile : Base
    {
        public Profile() { }

        [HttpGet("GetById")]
        public string GetById(int? id)
        {
            ProfileResultStruct result = new ProfileResultStruct();

            if (id == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }
            else
            {
                result.Status = 1;
                result.Data = new Models.User((int)id);
                result.Description = ResultDescription.ok;
            }

            return ToJson(result);

        }

        [HttpGet("ChangePassword")]
        public string ChangePassword(int? id, string oldPassword, string newPassword, string newPasswordConfirm)
        {
            ProfileResultStruct result = new ProfileResultStruct();
            result.Description = ResultDescription.invalidParams;
            if(id == null || newPasswordConfirm == null || newPassword == null || newPasswordConfirm == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }
            else
            {
                Models.User user = new Models.User((int)id);
                if (user.Password != oldPassword)
                {
                    result.Description = ResultDescription.passwordDidNotMatch;
                }
                if (newPassword != newPasswordConfirm)
                {
                    result.Description = ResultDescription.passwordsDidNotMatch;
                }
                if (newPassword == oldPassword)
                {
                    result.Description = ResultDescription.oldPasswordEqNewPassword;
                }
                if (result.Description == ResultDescription.invalidParams)
                {
                    if(user.ChangePassword((int)id, newPassword))
                    {
                        result.Status = 1;
                        result.Description = ResultDescription.ok;
                    }
                    else
                    {
                        result.Status = 0;
                        result.Description = ResultDescription.unknownError;
                    }
                }
                
            }

            return ToJson(result);
        }

        [HttpGet("ChangeSign")]
        public string ChangeSign(int? id, string sign)
        {
            ProfileResultStruct result = new ProfileResultStruct();

            if (id == null || sign == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }
            else
            {
                Models.User user = new Models.User((int)id);
                if (user.ChangeSign((int)id, sign))
                {
                    result.Status = 1;
                    result.Description = ResultDescription.ok;
                }
                else
                {
                    result.Status = 0;
                    result.Description = ResultDescription.unknownError;
                }
            }

            return ToJson(result);
        }

    }
}
