﻿using BlogApi.Controllers;
using BlogApi.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace BlogApi.Models
{
    [Route("api/[Controller]")]
    [ApiController]
    public class Search : Base
    {
        public Search() { }

        [HttpGet("Go")]
        public string Go(string searchText, int? startRecord = null, int? countRecords = null)
        {
            SearchResultStruct result = new SearchResultStruct();

            if (searchText == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }
            else
            {
                Theme theme = new Theme();
                Message message = new Message();

                List<Theme> themes = theme.Search(searchText, startRecord, countRecords);
                List<Message> messages = message.Search(searchText, startRecord, countRecords);

                result.Status = 1;
                result.Description = ResultDescription.ok;
                result.Themes = themes;
                result.Messages = messages;
                result.ThemesCount = theme.GetSearchCount(searchText);
                result.MessagesCount = message.GetSearchCount(searchText);
            }

            return ToJson(result);
        }
    }
}
