﻿using BlogApi.Utilities;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace BlogApi.Controllers
{
    [Route("api/[Controller]")]
    [ApiController]
    public class Theme : Base
    {
        Models.Message Message { get; set; }
        public Theme()
        {
            Message = new Models.Message();
        }

        [HttpGet("GetStruct")]
        public string GetStruct(int? themeId)
        {
            ThemeResultStruct result = new ThemeResultStruct();
            
            if(themeId == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }
            else
            {
                Models.Theme theme = new Models.Theme();

                result.Status = 1;
                result.Description = ResultDescription.ok;
                result.Data = Message.GetList((int)themeId);
                result.SectionId = theme.GetSectionIdByThemeId((int)themeId);
                result.ThemeId = (int)themeId;
                result.ThemeName = theme.GetName((int)themeId);
            }

            return ToJson(result);
        }

        [HttpGet("AddMessage")]
        public string AddMessage(int? userId, int? themeId, string text)
        {
            ThemeResultStruct result = new ThemeResultStruct();

            if (userId == null || themeId == null || text == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }
            else
            {
                Models.Message message = new Models.Message();
                if(message.Add((int)userId, (int)themeId, text))
                {
                    result.Status = 1;
                    result.Description = ResultDescription.ok;
                }
                else
                {
                    result.Status = 0;
                    result.Description = ResultDescription.unknownError;
                }
            }

            return ToJson(result);
        }

        [HttpGet("HideMessage")]
        public string HideMessage(int? userId, int? messageId)
        {
            ThemeResultStruct result = new ThemeResultStruct();

            if (userId == null || messageId == null)
            {
                result.Status = 0;
                result.Description = ResultDescription.invalidParams;
            }
            else
            {
                Models.Message message = new Models.Message();
                if (message.Hide((int)userId, (int)messageId))
                {
                    result.Status = 1;
                    result.Description = ResultDescription.ok;
                }
                else
                {
                    result.Status = 0;
                    result.Description = ResultDescription.fewRights;
                }

            }

            return ToJson(result);
        }
    }
}
