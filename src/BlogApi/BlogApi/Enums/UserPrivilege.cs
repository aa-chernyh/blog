﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlogApi.Enums
{
    public enum UserPrivilege
    {
        Administrator = 0,
        User = 3
    }

    public class Privilege
    {
        public static string GetName(UserPrivilege privilege)
        {
            string privilegeName;
            switch (privilege)
            {
                case UserPrivilege.Administrator:
                    privilegeName = "Администратор";
                    break;
                case UserPrivilege.User:
                    privilegeName = "Пользователь";
                    break;
                default:
                    privilegeName = "Пользователь";
                    break;
            }

            return privilegeName;
        }
    }
}
