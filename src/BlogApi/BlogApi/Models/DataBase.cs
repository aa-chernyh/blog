﻿using MySql.Data.MySqlClient;
using System;

namespace BlogApi.Models
{
    public class DataBase
    {
        private MySqlConnection _db = null;

        public DataBase() {
            
        }

        private void SetSqlConnection()
        {
            string host = "localhost";
            string database = "blog";
            string username = "backend";
            string password = "backend";

            string connString = "Server=" + host + "; Database=" + database + "; Username=" + username + "; Password=" + password + ";";
            _db = new MySqlConnection(connString);
            _db.Open();
        }

        public bool CastQueryNonSelect(string query)
        {
            bool result = false;
            try
            {
                SetSqlConnection();
                MySqlCommand res = new MySqlCommand(query, _db);

                result = res.ExecuteNonQuery() > 0;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                CloseSqlConnection();
            }
            
            return result;
        }

        public MySqlDataReader getSqlReader(string query)
        {
            SetSqlConnection();
            MySqlCommand res = new MySqlCommand(query, _db);
            return res.ExecuteReader();
        }

        public object getSqlScalar(string query)
        {
            object result = 0;

            try
            {
                SetSqlConnection();
                MySqlCommand res = new MySqlCommand(query, _db);

                result = res.ExecuteScalar();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                CloseSqlConnection();
            }

            return result;
        }

        public void CloseSqlConnection()
        {
            _db.Close();
        }

    }
}
