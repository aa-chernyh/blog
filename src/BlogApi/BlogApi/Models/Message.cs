﻿using BlogApi.Enums;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace BlogApi.Models
{
    public class Message : DataBase
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string CreationDate { get; set; }
        public string UserName { get; set; }
        public string UserSign { get; set; }
        public int ThemeId { get; set; }
        public int UserId { get; set; }

        public Message() {

        }

        public List<Message> GetList(int themeId)
        {
            List<Message> themes = new List<Message>();

            try
            {
                MySqlDataReader reader = getSqlReader($"SELECT m.*, u.name as 'userName', u.sign as 'userSign' " +
                                                      $"FROM message m " +
                                                      $"LEFT JOIN user u ON m.userId = u.id " +
                                                      $"WHERE m.`themeId` = {themeId} AND m.`isHide` != 1 " +
                                                      $"ORDER BY m.id DESC");
                while (reader.Read())
                {
                    Message theme = new Message
                    {
                        Id = reader.GetInt32("id"),
                        Text = reader.GetString("text"),
                        CreationDate = reader.GetDateTime("creationDate").ToString(),
                        UserName = reader.GetString("userName"),
                        UserSign = reader.GetString("userSign"),
                        ThemeId = reader.GetInt32("themeId"),
                        UserId = reader.GetInt32("userId")
                    };

                    themes.Add(theme);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                CloseSqlConnection();
            }

            return themes;
        }

        public bool Add(int userId, int themeId, string text)
        {
            return CastQueryNonSelect($"INSERT INTO `message` (`id`, `text`, `creationDate`, `isHide`, `themeId`, `userId`) VALUES (NULL, '{text}', CURRENT_TIMESTAMP, '0', '{themeId}', '{userId}');");
        }

        public bool Hide(int userId, int messageId)
        {
            User user = new User(userId);
            if (user.Privilege != Privilege.GetName(UserPrivilege.Administrator))
            {
                return false;
            }
            return CastQueryNonSelect($"UPDATE `message` SET `isHide` = '1' WHERE `message`.`id` = {messageId};");
        }

        public List<Message> Search(string searchText, int? startRecord = null, int? countRecords = null)
        {
            List<Message> result = new List<Message>();

            string query = $"SELECT themeId, text FROM message WHERE text LIKE '%{searchText}%'";
            if (startRecord != null && countRecords != null)
            {
                query += $" LIMIT {startRecord}, {countRecords}";
            }
            else
            {
                if (countRecords != null)
                {
                    query += $" LIMIT {countRecords}";
                }
            }

            try
            {
                MySqlDataReader reader = getSqlReader(query);
                while (reader.Read())
                {
                    Message message = new Message
                    {
                        ThemeId = reader.GetInt32("themeId"),
                        Text = reader.GetString("text")
                    };
                    result.Add(message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                CloseSqlConnection();
            }

            return result;
        }

        public int GetSearchCount(string searchText)
        {
            return (int)(long)getSqlScalar($"SELECT COUNT(*) FROM message WHERE text LIKE '%{searchText}%'");
        }
    }
}
