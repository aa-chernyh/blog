﻿using BlogApi.Enums;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace BlogApi.Models
{
    public class Section : DataBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreationDate { get; set; }
        public int UserId { get; set; }
        public List<Theme> Themes { get; set; }

        public Section() { }

        public List<Section> GetList()
        {
            List<Section> sections = new List<Section>();
            try
            {
                MySqlDataReader reader = getSqlReader($"SELECT * FROM `section` WHERE `isHide` != 1");
                while (reader.Read())
                {
                    Section section = new Section
                    {
                        Id = reader.GetInt32("id"),
                        Name = reader.GetString("name"),
                        CreationDate = reader.GetDateTime("creationDate").ToString(),
                        UserId = reader.GetInt32("userId")
                    };

                    sections.Add(section);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                CloseSqlConnection();
            }

            return sections;
        }

        public bool Create(int userId, string name)
        {
            User user = new User(userId);
            if(user.Privilege != Privilege.GetName(UserPrivilege.Administrator))
            {
                return false;
            }

            return CastQueryNonSelect($"INSERT INTO `section` (`id`, `name`, `creationDate`, `isHide`, `userId`) VALUES (NULL, '{name}', CURRENT_TIMESTAMP, 0, '{userId}')");
        }
    }
}
