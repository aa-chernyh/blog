﻿using BlogApi.Enums;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace BlogApi.Models
{
    public class Theme : DataBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CreationDate { get; set; }
        public string LastAuthor { get; set; }
        public string LastMessage { get; set; }
        public int MessagesCount { get; set; }
        public int SectionId { get; set; }
        public int UserId { get; set; }

        public List<Theme> GetList()
        {
            List<Theme> themes = new List<Theme>();
            
            try
            {
                MySqlDataReader reader = getSqlReader($"SELECT t.*, " +
                    $"(SELECT count(*) FROM message m WHERE m.themeId = t.id) as 'messageCount', " +
                    $"(SELECT name FROM user u WHERE u.id = (SELECT userId FROM message m WHERE m.themeId = t.id ORDER BY m.creationDate DESC LIMIT 1)) as 'lastAuthor', " +
                    $"(SELECT text FROM message m WHERE m.themeId = t.id ORDER BY m.creationDate DESC LIMIT 1) as 'lastMessage', " +
                    $"(SELECT count(*) FROM message m WHERE m.themeId = t.id) as 'MessagesCount' " +
                    $"FROM theme t " +
                    $"JOIN section s ON t.sectionId = s.id " +
                    $"WHERE t.`isHide` != 1 " +
                    $"ORDER BY t.id DESC");
                while (reader.Read())
                {
                    Theme theme = new Theme();
                    theme.Id = reader.GetInt32("id");
                    theme.Name = reader.GetString("name");
                    theme.CreationDate = reader.GetDateTime("creationDate").ToString();
                    try
                    {
                        theme.LastAuthor = reader.GetString("lastAuthor");
                    }
                    catch
                    {
                        theme.LastAuthor = "Нет автора";
                    }
                    try
                    {
                        theme.LastMessage = reader.GetString("lastMessage");
                    }
                    catch
                    {
                        theme.LastMessage = "Нет сообщений";
                    }
                    theme.MessagesCount = reader.GetInt32("MessagesCount");
                    theme.SectionId = reader.GetInt32("sectionId");
                    theme.UserId = reader.GetInt32("userId");

                    themes.Add(theme);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                CloseSqlConnection();
            }

            return themes;
        }

        public bool Create(int userId, int sectionId, string name)
        {
            return CastQueryNonSelect($"INSERT INTO `theme` (`id`, `name`, `creationDate`, `isHide`, `sectionId`, `userId`) VALUES (NULL, '{name}', CURRENT_TIMESTAMP, '0', '{sectionId}', '{userId}');");
        }

        public bool Hide(int userId, int themeId)
        {
            User user = new User(userId);
            if (user.Privilege != Privilege.GetName(UserPrivilege.Administrator))
            {
                return false;
            }
            return CastQueryNonSelect($"UPDATE `theme` SET `isHide` = '1' WHERE `theme`.`id` = {themeId};");
        }

        public int GetSectionIdByThemeId(int themeId)
        {
            return (int)getSqlScalar($"SELECT sectionId FROM `theme` WHERE id = {themeId}");
        }

        public string GetName(int themeId)
        {
            return (string)getSqlScalar($"SELECT name FROM theme WHERE id = {themeId};");
        }

        public List<Theme> Search(string searchText, int? startRecord = null, int? countRecords = null)
        {
            List<Theme> result = new List<Theme>();

            string query = $"SELECT id, name FROM `theme` WHERE name LIKE '%{searchText}%'";
            if(startRecord != null && countRecords != null)
            {
                query += $" LIMIT {startRecord}, {countRecords}";
            }
            else
            {
                if (countRecords != null)
                {
                    query += $" LIMIT {countRecords}";
                }
            }

            try
            {
                MySqlDataReader reader = getSqlReader(query);
                while (reader.Read())
                {
                    Theme theme = new Theme
                    {
                        Id = reader.GetInt32("id"),
                        Name = reader.GetString("name")
                    };
                    result.Add(theme);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                CloseSqlConnection();
            }

            return result;
        }
        public int GetSearchCount(string searchText)
        {
            return (int)(long)getSqlScalar($"SELECT COUNT(*) as 'count' FROM `theme` WHERE name LIKE '%{searchText}%'");
        }
    }
}
