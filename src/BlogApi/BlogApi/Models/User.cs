﻿using BlogApi.Enums;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;

namespace BlogApi.Models
{
    public class User : DataBase
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Privilege { get; set; }
        public string Sign { get; set; }
        public string RegDate { get; set; }
        public int ThemesCount { get; set; }
        public int MessagesCount { get; set; }

        public User() { }
        public User(int Id)
        {
            GetUserById(Id);
        }
        public User(string email)
        {
            GetUserByEmail(email);
        }

        public User GetUserById(int id)
        {
            try
            {
                MySqlDataReader reader = getSqlReader($"SELECT *, " +
                                            $"(SELECT count(*) FROM theme t WHERE t.userId = u.id) as 'themesCount', " +
                                            $"(SELECT count(*) FROM message m WHERE m.userId = u.id) as 'messagesCount' " +
                                            $"FROM user u " +
                                            $"WHERE u.id = {id}");
                reader.Read();
                this.Id = reader.GetInt32("id");
                Email = reader.GetString("email");
                Name = reader.GetString("name");
                Password = reader.GetString("password");
                Privilege = Enums.Privilege.GetName((UserPrivilege)reader.GetInt32("privilege"));
                Sign = reader.GetString("sign");
                RegDate = reader.GetDateTime("regDate").ToString();
                ThemesCount = reader.GetInt32("themesCount");
                MessagesCount = reader.GetInt32("messagesCount");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                CloseSqlConnection();
            }

            return this;
        }

        public User GetUserByEmail(string email)
        {
            try
            {
                MySqlDataReader reader = getSqlReader($"SELECT *, " +
                                            $"(SELECT count(*) FROM theme t WHERE t.userId = u.id) as 'themesCount', " +
                                            $"(SELECT count(*) FROM message m WHERE m.userId = u.id) as 'messagesCount' " +
                                            $"FROM user u " +
                                            $"WHERE u.email = '{email}'");
                reader.Read();
                this.Id = reader.GetInt32("id");
                Email = reader.GetString("email");
                Name = reader.GetString("name");
                Password = reader.GetString("password");
                Privilege = Enums.Privilege.GetName((UserPrivilege)reader.GetInt32("privilege"));
                Sign = reader.GetString("sign");
                RegDate = reader.GetDateTime("regDate").ToString();
                ThemesCount = reader.GetInt32("themesCount");
                MessagesCount = reader.GetInt32("messagesCount");
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
            finally
            {
                CloseSqlConnection();
            }

            return this;
        }

        public bool ChangePassword(int id, string newPassword)
        {
            return CastQueryNonSelect($"UPDATE `user` SET `password` = '{newPassword}' WHERE `user`.`id` = {id};");
        }

        public bool ChangeSign(int id, string sign)
        {
            return CastQueryNonSelect($"UPDATE `user` SET `sign` = '{sign}' WHERE `user`.`id` = {id};");
        }

        public bool Registration(string name, string email, string password, UserPrivilege privilege)
        {
            string defaultSign = "Всем привет! Я здесь новенький!";
            bool result = CastQueryNonSelect($"INSERT INTO `user` (`id`, `email`, `name`, `password`, `privilege`, `sign`, `regDate`) " +
                                      $"VALUES (NULL, '{email}', '{name}', '{password}', '{(int)privilege}', '{defaultSign}', CURRENT_TIMESTAMP);");

            GetUserByEmail(email);
            return result;
        }
    }
}
