﻿namespace BlogApi.Utilities
{
    public class BaseResultStruct
    {
        public byte Status;
        public string Description;
    }
}
