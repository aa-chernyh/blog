﻿using BlogApi.Models;
using System.Collections.Generic;

namespace BlogApi.Utilities
{
    public class EnterResultStruct : BaseResultStruct
    {
        public List<Section> Data;
    }
}
