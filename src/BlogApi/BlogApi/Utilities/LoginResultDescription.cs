﻿using BlogApi.Models;

namespace BlogApi.Utilities
{
    public class LoginResultStruct : BaseResultStruct
    {
        public User Data;
    }
}