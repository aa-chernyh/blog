﻿using BlogApi.Models;

namespace BlogApi.Utilities
{
    public class ProfileResultStruct : BaseResultStruct
    {
        public User Data;
    }
}