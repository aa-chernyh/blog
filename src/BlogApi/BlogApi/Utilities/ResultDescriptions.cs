﻿namespace BlogApi.Utilities
{

    public class ResultDescription
    {
        public static string invalidParams = "Неверные параметры!";
        public static string unknownError = "Ошибка!";
        public static string ok = "OK";
        public static string passwordDidNotMatch = "Пароль неверен!";
        public static string passwordsDidNotMatch = "Пароли не совпадают!";
        public static string oldPasswordEqNewPassword = "Новый и старый пароли совпадают!";
        public static string emailExists = "Такой email уже существует!";
        public static string fewRights = "Недостаточно прав!";
        public static string sectionExists = "Такой раздел уже существует!";
    }
}
