﻿using BlogApi.Models;
using System.Collections.Generic;

namespace BlogApi.Utilities
{
    public class SearchResultStruct : BaseResultStruct
    {
        public List<Theme> Themes;
        public List<Message> Messages;
        public int ThemesCount;
        public int MessagesCount;
    }
}
