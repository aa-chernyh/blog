﻿using BlogApi.Models;
using System.Collections.Generic;

namespace BlogApi.Utilities
{
    public class ThemeResultStruct : BaseResultStruct
    {
        public List<Message> Data;
        public int SectionId;
        public int ThemeId;
        public string ThemeName;
    }
}