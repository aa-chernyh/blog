<?php

class Route {  
    public static function Start() {
        $route = $_REQUEST['route'];
        
        if(!isset($_COOKIE["Id"]) && $route != "login"){
            unset($_GET['route']);
            $redir = !empty($route) ? "?redir=" . $route : "";
            $get = !empty($_GET) && !empty($redir) ? "&" . http_build_query($_GET) : "";
            header("Location: login" . $redir . $get);
            die;
        }
        if(empty($route)){
            $route = "enter";
        }
        
        $fileName = ucfirst(isset($_GET['redir']) && $route != "login" ? $redir : $route);
        $controllerPath = "controllers/" . $fileName . ".php";
        if (file_exists($controllerPath)) {
            require_once $controllerPath;
            $controller = new $fileName;
        } else {
            require_once 'controllers/Error.php';
            $controller = new Error;
        }
    }
}

?>
