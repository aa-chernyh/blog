-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 30 2019 г., 22:28
-- Версия сервера: 5.6.41
-- Версия PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `blog`
--

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `text` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isHide` tinyint(1) DEFAULT NULL,
  `themeId` int(11) NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `message`
--

INSERT INTO `message` (`id`, `text`, `creationDate`, `isHide`, `themeId`, `userId`) VALUES
(22, 'Интересует процессор AMD для игр. Так вот, подождать ли до июля, когда обещают выпустить новые процессоры, или купить сейчас процессор ryzen, 2000 серии?', '2019-05-30 15:12:36', 0, 19, 23),
(23, 'Если время терпит, то лучше точно подождать.', '2019-05-30 15:13:46', 0, 19, 24),
(24, 'Лично я вот жду.', '2019-05-30 15:14:41', 0, 19, 25),
(25, 'Интел с переходом на новый техпроцесс добавит максимум 20% к ядру, получается чтобы догнать 3900Х ей нада сделать 10 ядерник айс-лайк i9, т.е. АМД заранее сделала ответ, еще и козырь есть)\r\nдаже если интел выпустит 12 ядерный CPU он наверное все равно буд', '2019-05-30 15:16:25', 0, 20, 23),
(26, 'Щас вам напишут что многопоток нужен только стримерам и синебенчи гонять. Да и вообще зачем мультипоток? Однопоток роляет на фпс а память и многопоток не роляет и нипаиграть.', '2019-05-30 15:16:51', 0, 20, 25),
(27, 'Так фанаты Интела всё равно в играх мерят, которым часто достаточно 4 ядер на 5ГГЦ... Они будут покупать в любом случае, их не переубедить, у них комп только для игр и используется.', '2019-05-30 15:17:12', 0, 20, 24),
(28, 'В этой теме можно свободно вести извечный спор &quot;кто же круче?&quot;. Холливары приветствуются (в пределах правил естественно).', '2019-05-30 15:21:00', 0, 21, 23),
(29, 'Всегда покупал видеокарты Radeon и Geforce по очереди. Сказать кто лучший не возможно. Можно только в данный момент времени решить какая карта по деньгам вам лучше всего подходит.', '2019-05-30 15:21:20', 0, 21, 25),
(30, 'Никогда не покупал видеокарты Radeon. Поскольку знаю кто лучший. Готов переплатить за меньшую производительность, поскольку мне важнее качество, надёжность, и мой опыт.\r\nА вообще 3dfx круче. Ибо они сделали переворот в мире видеокарт, после чего с потроха', '2019-05-30 15:21:38', 0, 21, 24),
(31, 'asdasdasd', '2019-05-30 15:27:55', 0, 21, 23),
(32, 'Сейчас все большую популярность набирают двухканальные «настольные» серии чипсетов (Е7205). В чем плюсы и минусы реализации двухканальности работы с памятью от Intel по сравнению с другими производителями?', '2019-05-30 15:31:48', 0, 22, 23),
(33, 'Чипсеты от Intel обладают сбалансированной пропускной способностью по любой из шин доступа, будь то процессорная шина, память или ввод-вывод. Конечно, результирующая производительность зависит и от BIOS, и от разводки платы, и от корректности установки др', '2019-05-30 15:37:00', 0, 22, 25),
(34, 'У меня следующий вопрос: «Почему частота системной шины увеличивается гораздо реже, чем частота процессора, ведь на производительность системы в целом большее влияние оказывает вторая величина, нежели первая?»', '2019-05-30 15:37:37', 0, 22, 24),
(35, 'Производительность системы есть результат вклада целого ряда факторов. Часть из них, такая, как частота шины, требует разработки и проверки таких приемов проектирования, которые обеспечили бы надежность всей системы.', '2019-05-30 15:37:52', 0, 22, 23);

-- --------------------------------------------------------

--
-- Структура таблицы `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isHide` tinyint(4) DEFAULT NULL,
  `userId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `section`
--

INSERT INTO `section` (`id`, `name`, `creationDate`, `isHide`, `userId`) VALUES
(7, 'Процессоры', '2019-05-30 15:12:15', 0, 23),
(8, 'Видеокарты', '2019-05-30 15:18:45', 0, 23),
(9, 'Материнские платы', '2019-05-30 15:30:26', 0, 23);

-- --------------------------------------------------------

--
-- Структура таблицы `theme`
--

CREATE TABLE `theme` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `creationDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isHide` tinyint(4) DEFAULT NULL,
  `sectionId` int(11) NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `theme`
--

INSERT INTO `theme` (`id`, `name`, `creationDate`, `isHide`, `sectionId`, `userId`) VALUES
(19, 'Ждать ли Ryzen 3000', '2019-05-30 15:12:23', 0, 7, 23),
(20, 'Новым 7-нм процессорам AMD Ryzen 3000 полагаются и новые маркировки', '2019-05-30 15:15:42', 0, 7, 23),
(21, 'Холиварня: AMD vs NVIDIA', '2019-05-30 15:20:50', 0, 8, 23),
(22, 'Вопросы и ответы по системным платам и чипсетам от Intel', '2019-05-30 15:31:39', 0, 9, 23),
(23, 'Какую материнку купить?', '2019-05-30 15:40:26', 0, 9, 24);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` char(80) NOT NULL,
  `name` char(127) NOT NULL,
  `password` char(63) NOT NULL,
  `privilege` int(1) UNSIGNED NOT NULL DEFAULT '0',
  `sign` varchar(255) DEFAULT NULL,
  `regDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `password`, `privilege`, `sign`, `regDate`) VALUES
(23, 'aa-chernyh@mail.ru', 'Александр', '81dc9bdb52d04dc20036dbd8313ed055', 0, 'Приветствую Вас, товарищи!', '2019-05-30 15:11:48'),
(24, 'oleg@mail.ru', 'Олег заезжий', '81dc9bdb52d04dc20036dbd8313ed055', 3, 'Системный администратор завода &quot;Прогресс&quot; негодует...', '2019-05-30 15:13:37'),
(25, 'bitterleaf@mail.ru', 'Bitterleaf', '81dc9bdb52d04dc20036dbd8313ed055', 3, 'Всем привет! Я здесь новенький!', '2019-05-30 15:14:27'),
(28, 'test@mail.ru', 'Тестовая регистрация', '81dc9bdb52d04dc20036dbd8313ed055', 3, 'Всем привет! Я здесь новенький!', '2019-05-30 17:50:30'),
(29, 'test111@mail.ru', 'test', '7a07ca426f1cfde22a2f9c839c8d9aec', 3, 'Всем привет! Я здесь новенький!', '2019-05-30 17:51:11');

-- --------------------------------------------------------

--
-- Структура таблицы `user_avatar`
--

CREATE TABLE `user_avatar` (
  `id` int(11) NOT NULL,
  `type` varchar(25) NOT NULL DEFAULT '',
  `image` blob NOT NULL,
  `size` varchar(25) NOT NULL DEFAULT '',
  `ctgy` varchar(25) NOT NULL DEFAULT '',
  `name` varchar(50) NOT NULL DEFAULT '',
  `userId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`),
  ADD KEY `themeId` (`themeId`),
  ADD KEY `userId` (`userId`),
  ADD KEY `themeId_text` (`themeId`,`text`);

--
-- Индексы таблицы `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `userId` (`userId`);

--
-- Индексы таблицы `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`),
  ADD KEY `sectionId` (`sectionId`),
  ADD KEY `id_name` (`id`,`name`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Индексы таблицы `user_avatar`
--
ALTER TABLE `user_avatar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userId` (`userId`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT для таблицы `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `theme`
--
ALTER TABLE `theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT для таблицы `user_avatar`
--
ALTER TABLE `user_avatar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `message_ibfk_1` FOREIGN KEY (`themeId`) REFERENCES `theme` (`id`),
  ADD CONSTRAINT `message_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `section_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `theme`
--
ALTER TABLE `theme`
  ADD CONSTRAINT `theme_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `theme_ibfk_2` FOREIGN KEY (`sectionId`) REFERENCES `section` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_avatar`
--
ALTER TABLE `user_avatar`
  ADD CONSTRAINT `user_avatar_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
