<?php
require_once 'libs/Encoding.php';

class API {

    private static $url = "http://localhost:44444/api/";

    public static function sendRequest($method, $params) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::$url . $method . "?" . http_build_query($params));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 15); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 15); 
        
        if (!($result = curl_exec($ch))) {
            $error = curl_error($ch) == "" ? "wrong API method or lost param" : curl_error($ch);
            echo "cURL error: " . $error;
            die;
        }

        curl_close($ch);
        return json_decode($result);
    }

}
