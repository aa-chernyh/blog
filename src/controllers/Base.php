<?php

require_once 'libs/smarty-3.1.33/libs/Smarty.class.php';
require_once 'libs/Encoding.php';
require_once 'API.php';


class Base extends Smarty {

    private $className = "Error";
    protected $request = array();
    protected $title = "Интересное";

    public function __construct($className) {
        parent::__construct();
        $this->template_dir = 'views/templates/';
        $this->compile_dir = 'libs/smarty-3.1.33/templates_compiled/';
        $this->config_dir = 'libs/smarty-3.1.33/configs/';
        $this->cache_dir = 'libs/smarty-3.1.33/cache/';
        $this->caching = false;

        $this->className = $className;
        
        foreach ($_REQUEST as $key => $value) {
            $this->request[$key] = htmlspecialchars($value);
        }
    }

    protected function displayResult() {
        $this->assign("template", $this->getTemplatePath());
        $this->assign("stylesPath", $this->getStylesPath());
        $this->assign("scriptsPath", $this->getScriptsPath());
        $this->assign("title", $this->getTitle());
        $this->assign("currentYear", date("Y"));
        
        $params = array(
            "Id" => $_COOKIE['Id']
        );
        $user = API::sendRequest("Profile/GetById", $params);
        $this->assign("User", (array)$user->Data);
        
        $this->display('views/templates/Main.tpl');
    }

    protected function getStylesPath() {
        $filePath = "views/css/" . $this->className . ".css";
        return file_exists($filePath) ? $filePath : NULL;
    }

    protected function getScriptsPath() {
        $filePath = "views/js/" . $this->className . ".js";
        return file_exists($filePath) ? $filePath : NULL;
    }

    protected function getTemplatePath() {
        $filePath = "views/templates/" . $this->className . ".tpl";
        return file_exists($filePath) ? $filePath : NULL;
    }

    protected function getTitle() {
        return $this->title;
    }
    
    protected function objectToArrayRecursive($object) {
        $result = $object;
        if(is_object($object) || is_array($object)){
            $object = (array)$object;
            $result = array();
            foreach($object as $key => $value){
                $result[$key] = $this->objectToArrayRecursive($value);
            }
        }
        
        return $result;
    }
}