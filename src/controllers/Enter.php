<?php
require_once 'Base.php';

class Enter extends Base {
    public function __construct() {
        parent::__construct(__CLASS__);
        $this->doAction();
        $this->assignTemplate();
        $this->displayResult();
    }
    
    private function assignTemplate(){
        $this->title = "Главная";
        
        $sectionsStruct = API::sendRequest("Enter/GetStruct", array());
        $this->assign($this->objectToArrayRecursive($sectionsStruct));
    }
    
    private function doAction() {
        switch ($this->request['action']) {
            case "createSection":
                $this->actionCreateSection();
                break;
            case "createTheme":
                $this->actionCreateTheme();
                break;
            case "deleteTheme":
                $this->actionDeleteTheme();
            default:
                break;
        }
    }
    
    private function actionCreateSection() {
        $params = array(
            "UserId" => $this->request['userId'],
            "Name" => $this->request['name']
        );
        API::sendRequest("Enter/CreateSection", $params);
    }
    
    private function actionCreateTheme() {
        $params = array(
            "UserId" => $this->request['userId'],
            "SectionId" => $this->request['sectionId'],
            "Name" => $this->request['name']
        );
        API::sendRequest("Enter/CreateTheme", $params);
    }
    
    private function actionDeleteTheme() {
        $params = array(
            "UserId" => $this->request['userId'],
            "ThemeId" => $this->request['themeId']
        );
        API::sendRequest("Enter/HideTheme", $params);
    }
}
