<?php
require_once 'Base.php';

class Error extends Base {
    
    public function __construct() {
        parent::__construct(__CLASS__);
        $this->assignTemplate();
        $this->displayResult();
    }
    
    private function assignTemplate() {
        $this->title = "Ошибка";
    }
}
