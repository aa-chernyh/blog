<?php

require_once 'Base.php';

class Login extends Base {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->doAction();
        $this->assignTemplate();
        $this->displayResult();
    }

    private function assignTemplate() {
        $this->title = "Войти";
        $this->assign("isLoginPage", true);

        $get = "";
        if (isset($_GET['redir'])) {
            unset($_GET['route']);
            $get .= "?" . http_build_query($_GET);
        }
        $this->assign("get", $get);
    }

    private function doAction() {
        switch ($this->request['action']) {
            case "login":
                $result = $this->actionLogin();
                $this->assign("isLoginSuccess", $result->Status);
                $this->assign("LoginErrorText", $result->Description);
                break;
            case "registration":
                $result = $this->actionRegistration();
                $this->assign("isRegisterFail", !$result->Status);
                $this->assign("RegisterErrorText", $result->Description);
                break;
            case "logout":
                $this->actionLogout();
                break;
            default:
                break;
        }
    }

    private function actionLogin() {
        $params = array(
            "Email" => $this->request['email'],
            "Password" => $this->request['password']
        );
        $this->assign($params);

        $params['Email'] = strtolower($params['Email']);
        $params['Password'] = md5($params['Password']);
        $result = API::sendRequest("Login/Go", $params);
        if ($result->Status) {
            $this->setCookieAndSendRedirParams($result->Data->Id);
        }
        return $result;
    }

    private function setCookieAndSendRedirParams($userId) {
        setcookie("Id", $userId);

        $route = "enter";
        if (isset($_GET['redir'])) {
            $route = $_GET['redir'];
            unset($_GET['route']);
            unset($_GET['redir']);
            $route .= "?" . http_build_query($_GET);
        }
        header("Location: " . $route);
    }

    private function actionRegistration() {
        $params = array(
            "Name" => $this->request['name'],
            "Email" => $this->request['email'],
            "Password" => $this->request['password'],
            "PasswordConfirm" => $this->request['passwordConfirm'],
        );
        $this->assign($params);

        $params['Email'] = strtolower($params['Email']);
        $params['Password'] = md5($params['Password']);
        $params['PasswordConfirm'] = md5($params['PasswordConfirm']);
        $result = API::sendRequest("Login/Registration", $params);
        if ($result->Status) {
            $this->setCookieAndSendRedirParams($result->Data->Id);
        }
        return $result;
    }

    private function actionLogout() {
        unset($_COOKIE['Id']);
        setcookie('Id', null, -1, '/');
    }

}
