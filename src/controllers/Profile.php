<?php

require_once 'Base.php';

class Profile extends Base {

    public function __construct() {
        parent::__construct(__CLASS__);
        $this->doAction();
        $this->assignTemplate();
        $this->displayResult();
    }

    private function assignTemplate() {
        $this->title = "Мой профиль";

        $params = array(
            "Id" => $_COOKIE['Id']
        );
    }

    private function doAction() {
        switch ($this->request['action']) {
            case "changePassword":
                $result = $this->actionChangePassword();
                $this->assign("ChangePasswordResult", (bool)$result->Status);
                $this->assign("ChangePasswordErrorText", $result->Description);
                break;
            case "changeSign":
                $result = $this->actionChangeSign();
                $this->assign("ChangeSignResult", (bool)$result->Status);
                $this->assign("ChangeSignErrorText", $result->Description);
                break;
            default:
                break;
        }
    }
    
    private function actionChangePassword() {
        $params = array(
            "Id" => $this->request['userId'],
            "OldPassword" => $this->request['oldPassword'],
            "NewPassword" => $this->request['newPassword'],
            "NewPasswordConfirm" => $this->request['newPasswordConfirm']
        );
        $this->assign($params);
        
        $params['OldPassword'] = md5($params['OldPassword']);
        $params['NewPassword'] = md5($params['NewPassword']);
        $params['NewPasswordConfirm'] = md5($params['NewPasswordConfirm']);
        return API::sendRequest("Profile/ChangePassword", $params);
    }
    
    private function actionChangeSign() {
        $params = array(
            "Id" => $this->request['userId'],
            "Sign" => $this->request['sign']
        );
        $this->assign($params);
        
        return API::sendRequest("Profile/ChangeSign", $params);
    }

}
