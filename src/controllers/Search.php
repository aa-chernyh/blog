<?php
require_once 'Base.php';

class Search extends Base {
    public function __construct() {
        parent::__construct(__CLASS__);
        $this->assignTemplate();
        $this->displayResult();
    }
    
    private function assignTemplate(){
        $this->title = "Поиск";
        $this->assign("searchText", $this->request['searchText']);
        
        $recordsPerPage = 7;
        $page = isset($this->request['page']) ? $this->request['page'] : 1;
        $startRecord = ($page-1) * $recordsPerPage;
        
        $params = array(
            "searchText" => $this->request['searchText'],
            "startRecord" => $startRecord,
            "countRecords" => $recordsPerPage,
        );
        $result = API::sendRequest("Search/Go", $params);
        $this->assign("Themes", $this->markText($params['searchText'], $this->objectToArrayRecursive($result->Themes), "Name"));
        $this->assign("Messages", $this->markText($params['searchText'], $this->objectToArrayRecursive($result->Messages), "Text"));
        
        $biggerCount = $result->ThemesCount > $result->MessagesCount ? $result->ThemesCount : $result->MessagesCount;
        $countPages = intdiv($biggerCount, $recordsPerPage);
        if(fmod($biggerCount, $recordsPerPage) > 0) {
            $countPages++;
        }
        $this->assign("countPages", $countPages);
    }
    
    private function markText($searchText, $array, $fieldName) {
        foreach($array as $key => $value) {
            $position = stripos($value[$fieldName], $searchText);
            $lengthOfSearchText = strlen($searchText);
            
            $textBeforePosition = substr($value[$fieldName], 0, $position);
            $markedText = "<mark>" . substr($value[$fieldName], $position, $lengthOfSearchText) . "</mark>";
            $textAfterPosition = substr($value[$fieldName], $position + $lengthOfSearchText);
            
            $array[$key][$fieldName] = $textBeforePosition . $markedText . $textAfterPosition;
        }
        return $array;
    }
}