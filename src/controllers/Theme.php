<?php

require_once 'Base.php';

class Theme extends Base {

    public function __construct() {
        parent::__construct(__CLASS__);
        if (!isset($this->request['id'])) {
            header("Location: error");
        }
        $this->doAction();
        $this->assignTemplate();
        $this->displayResult();
    }

    private function assignTemplate() {
        $this->title = "";

        $params = array(
            "ThemeId" => $this->request['id']
        );
        $themeStruct = API::sendRequest("Theme/GetStruct", $params);
        $this->assign($this->objectToArrayRecursive($themeStruct));
    }

    private function doAction() {
        switch ($this->request['action']) {
            case "addMessage":
                $this->actionAddMessage();
                break;
            case "deleteMessage":
                $this->actionDeleteMessage();
                break;
            default:
                break;
        }
    }

    private function actionAddMessage() {
        $params = array(
            "UserId" => $this->request['userId'],
            "ThemeId" => $this->request['themeId'],
            "Text" => $this->request['text']
        );
        
        API::sendRequest("Theme/AddMessage", $params);
    }
    
    private function actionDeleteMessage() {
        $params = array(
            "UserId" => $this->request['userId'],
            "MessageId" => $this->request['messageId']
        );
        
        API::sendRequest("Theme/HideMessage", $params);
    }

}
