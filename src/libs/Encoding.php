<?php

class Encoding {

    public static function Convert($str) {
        if (is_array($str) || is_object($str)) {
            $result = array();
            foreach ($str as $key => $value) {
                $result[$key] = Encoding::Convert($value);
            }
        } else {
            $result = iconv('utf-8', 'windows-1251', $str);
        }

        return $result;
    }

}

?>