<?php
/* Smarty version 3.1.33, created on 2019-05-28 14:24:06
  from 'W:\domains\blog\views\templates\Login.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5ced1a56bb8ad4_98358226',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '0b8bd5c50fb7954991dea81288660540ca0ae16b' => 
    array (
      0 => 'W:\\domains\\blog\\views\\templates\\Login.tpl',
      1 => 1559042644,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ced1a56bb8ad4_98358226 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="content__wrapper">
    <div class="loginPage">
        <div class="loginPage__login">
            <div class="loginPage__register__description">
                <h2>Вход</h2>
                <p class="loginPage__register__description__error red <?php if ($_smarty_tpl->tpl_vars['isLoginSuccess']->value) {?>hide<?php }?>"><?php echo $_smarty_tpl->tpl_vars['LoginErrorText']->value;?>
</p>
            </div>
            <form action="login<?php echo $_smarty_tpl->tpl_vars['get']->value;?>
" method="POST">
                <input type="text" name="email" placeholder="E-mail" class="loginPage__input">
                <input type="password" name="password" placeholder="Пароль" class="loginPage__input">
                <input type="hidden" name="action" value="login">
                <div>
                    <button type="button" class="button loginPage__login__submit" onclick="checkFieldsAndSubmit(this)">Войти</button>
                </div>
            </form>
        </div>
        <div class="loginPage__register">
            <div class="loginPage__register__description">
                <h2>Впервые здесь?</h2>
                <p class="<?php if ($_smarty_tpl->tpl_vars['isRegisterFail']->value) {?>hide<?php }?>">Моментальная регистрация</p>
                <p class="red <?php if (!$_smarty_tpl->tpl_vars['isRegisterFail']->value) {?>hide<?php }?>"><?php echo $_smarty_tpl->tpl_vars['RegisterErrorText']->value;?>
</p>
            </div>
            <form action="login<?php echo $_smarty_tpl->tpl_vars['get']->value;?>
" method="POST">
                <input type="text" name="name" placeholder="Ваше имя" class="loginPage__input">
                <input type="text" name="email" placeholder="E-mail" class="loginPage__input">
                <input type="password" name="password" placeholder="Пароль" class="loginPage__input">
                <input type="password" name="passwordConfirm" placeholder="Подтвердите пароль" class="loginPage__input">
                <input type="hidden" name="action" value="registration">
                <div>
                    <button type="button" class="button loginPage__register__submit" onclick="checkFieldsAndSubmit(this)">Закончить регистрацию</button>
                </div>
            </form>
        </div>
    </div>
</div><?php }
}
