<?php
/* Smarty version 3.1.33, created on 2019-05-29 22:49:00
  from 'W:\domains\blog\views\templates\Profile.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5ceee22c785a42_54161506',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '6e388ab75b55907797f6932c3c4dd874ecaf7942' => 
    array (
      0 => 'W:\\domains\\blog\\views\\templates\\Profile.tpl',
      1 => 1559159339,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5ceee22c785a42_54161506 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="content__wrapper">
    <div class="background">
        <div class="profile">
            <div class="profile__title">
                <h2>Профиль пользователя <?php echo $_smarty_tpl->tpl_vars['User']->value['Name'];?>
</h2>
            </div>
            <div class="profile__section flex">
                <div class="profile__section__avatar">
                    <img src="/src/viewsiews/img/defaultAvatar.png" alt="avatar" onmouseover="$('.profile__section__avatar__change').slideDown('');">
                    <div class="profile__section__avatar__change hide">
                        <form action="profile" enctype="multipart/form-data" method="POST">
                            <p class="bold pointer">
                                <input type="hidden" name="action" value="changeAvatar">
                                <input type="hidden" name="userId" value="<?php echo $_smarty_tpl->tpl_vars['User']->value['Id'];?>
">
                                <input type="file" value="Изменить аватарку" onchange="$('.profile__section__avatar__change form').submit();">
                            </p>
                        </form>
                    </div>
                </div>
                <div class="profile__section__password">
                    <p class="profile__section__password__title <?php if (isset($_smarty_tpl->tpl_vars['ChangePasswordResult']->value)) {?>hide<?php }?>">Сменить пароль:</p>
                    <p class="profile__section__password__title green <?php if (!$_smarty_tpl->tpl_vars['ChangePasswordResult']->value) {?>hide<?php }?>">Пароль успешно изменен!</p>
                    <p class="profile__section__password__title red <?php if ($_smarty_tpl->tpl_vars['ChangePasswordResult']->value) {?>hide<?php }?>"><?php echo $_smarty_tpl->tpl_vars['ChangePasswordErrorText']->value;?>
</p>
                    <form action="profile" method="POST">
                        <input type="password" name="oldPassword" class="input" placeholder="Старый пароль" value="<?php echo $_smarty_tpl->tpl_vars['OldPassword']->value;?>
">
                        <input type="password" name="newPassword" class="input" placeholder="Новый пароль" value="<?php echo $_smarty_tpl->tpl_vars['NewPassword']->value;?>
">
                        <input type="password" name="newPasswordConfirm" class="input" placeholder="Новый пароль ещё разок" value="<?php echo $_smarty_tpl->tpl_vars['NewPasswordConfirm']->value;?>
">
                        <input type="hidden" name="userId" value="<?php echo $_smarty_tpl->tpl_vars['User']->value['Id'];?>
">
                        <input type="hidden" name="action" value="changePassword">
                        <div>
                            <button type="button" class="button profile__section__password__confirm" onclick="checkFieldsAndSubmit(this);">Подтвердить</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="profile__section">
                <table class="profile__section__userInfo">
                    <tr>
                        <td>Email</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['User']->value['Email'];?>
</td>
                    </tr>
                    <tr>
                        <td>Дата регистрации</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['User']->value['RegDate'];?>
</td>
                    </tr>
                    <tr>
                        <td>Привилегии</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['User']->value['Privilege'];?>
</td>
                    </tr>
                    <tr>
                        <td>Сообщений написано</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['User']->value['MessagesCount'];?>
</td>
                    </tr>
                    <tr>
                        <td>Тем создано</td>
                        <td><?php echo $_smarty_tpl->tpl_vars['User']->value['ThemesCount'];?>
</td>
                    </tr>
                </table>
            </div>
            <div class="profile__section">
                <div class="profile__section__userSign">
                    <p class="profile__section__userSign__title <?php if (isset($_smarty_tpl->tpl_vars['ChangeSignResult']->value)) {?>hide<?php }?>">Подпись под сообщениями:</p>
                    <p class="profile__section__userSign__title green <?php if (!$_smarty_tpl->tpl_vars['ChangeSignResult']->value) {?>hide<?php }?>">Подпись успешно изменена!</p>
                    <p class="profile__section__userSign__title red <?php if ($_smarty_tpl->tpl_vars['ChangeSignResult']->value) {?>hide<?php }?>"><?php echo $_smarty_tpl->tpl_vars['ChangeSignErrorText']->value;?>
</p>
                    <div class="profile__section__userSign__change">
                        <form action="profile" method="POST">
                            <textarea class="input" name="sign"><?php echo $_smarty_tpl->tpl_vars['User']->value['Sign'];?>
</textarea>
                            <input type="hidden" name="userId" value="<?php echo $_smarty_tpl->tpl_vars['User']->value['Id'];?>
">
                            <input type="hidden" name="action" value="changeSign">
                            <div>
                                <button type="button" class="button profile__section__userSign__change__button" onclick="checkTextareaAndSubmit(this);">Подтвердить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php }
}
