<?php
/* Smarty version 3.1.33, created on 2019-05-30 16:38:54
  from 'W:\domains\blog\views\templates\Search.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cefdcee225889_23955169',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '9316af285585af0b95981ffa3630d191d03fbf3c' => 
    array (
      0 => 'W:\\domains\\blog\\views\\templates\\Search.tpl',
      1 => 1559223532,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cefdcee225889_23955169 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="content__wrapper">
    <div class="background">
        <div class="search">
            <?php if (!empty($_smarty_tpl->tpl_vars['Themes']->value) || !empty($_smarty_tpl->tpl_vars['Messages']->value)) {?>
                <?php if (!empty($_smarty_tpl->tpl_vars['Themes']->value)) {?>
                    <div class="search__section">
                        <div class="search__section__title">
                            <h2>Темы:</h2>
                        </div>
                        <?php
$__section_theme_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['Themes']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_theme_0_total = $__section_theme_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_theme'] = new Smarty_Variable(array());
if ($__section_theme_0_total !== 0) {
for ($__section_theme_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index'] = 0; $__section_theme_0_iteration <= $__section_theme_0_total; $__section_theme_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index']++){
?>
                            <div class="search__section__results">
                                <div class="search__section__results__text">
                                    <a href="theme?id=<?php echo $_smarty_tpl->tpl_vars['Themes']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index'] : null)]['Id'];?>
" target="_blank">
                                        <?php echo $_smarty_tpl->tpl_vars['Themes']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index'] : null)]['Name'];?>

                                    </a>
                                </div>
                            </div>
                        <?php
}
}
?>
                    </div>
                <?php }?>
                <?php if (!empty($_smarty_tpl->tpl_vars['Messages']->value)) {?>
                    <div class="search__section">
                        <div class="search__section__title">
                            <h2>Сообщения:</h2>
                        </div>
                        <div class="search__section__results">
                            <?php
$__section_message_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['Messages']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_message_1_total = $__section_message_1_loop;
$_smarty_tpl->tpl_vars['__smarty_section_message'] = new Smarty_Variable(array());
if ($__section_message_1_total !== 0) {
for ($__section_message_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_message']->value['index'] = 0; $__section_message_1_iteration <= $__section_message_1_total; $__section_message_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_message']->value['index']++){
?>
                                <div class="search__section__results__text">
                                    <a href="theme?id=<?php echo $_smarty_tpl->tpl_vars['Messages']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_message']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_message']->value['index'] : null)]['ThemeId'];?>
" target="_blank">
                                        <?php echo $_smarty_tpl->tpl_vars['Messages']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_message']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_message']->value['index'] : null)]['Text'];?>

                                    </a>
                                </div>
                            <?php
}
}
?>
                        </div>
                    </div>
                <?php }?>
                <span class="search__pagination">
                    <p>
                        <?php
$_smarty_tpl->tpl_vars['i'] = new Smarty_Variable(null, $_smarty_tpl->isRenderingCache);$_smarty_tpl->tpl_vars['i']->step = 1;$_smarty_tpl->tpl_vars['i']->total = (int) ceil(($_smarty_tpl->tpl_vars['i']->step > 0 ? $_smarty_tpl->tpl_vars['countPages']->value+1 - (1) : 1-($_smarty_tpl->tpl_vars['countPages']->value)+1)/abs($_smarty_tpl->tpl_vars['i']->step));
if ($_smarty_tpl->tpl_vars['i']->total > 0) {
for ($_smarty_tpl->tpl_vars['i']->value = 1, $_smarty_tpl->tpl_vars['i']->iteration = 1;$_smarty_tpl->tpl_vars['i']->iteration <= $_smarty_tpl->tpl_vars['i']->total;$_smarty_tpl->tpl_vars['i']->value += $_smarty_tpl->tpl_vars['i']->step, $_smarty_tpl->tpl_vars['i']->iteration++) {
$_smarty_tpl->tpl_vars['i']->first = $_smarty_tpl->tpl_vars['i']->iteration === 1;$_smarty_tpl->tpl_vars['i']->last = $_smarty_tpl->tpl_vars['i']->iteration === $_smarty_tpl->tpl_vars['i']->total;?>
                            <a href="search?searchText=<?php echo $_smarty_tpl->tpl_vars['searchText']->value;?>
&page=<?php echo $_smarty_tpl->tpl_vars['i']->value;?>
"><?php echo $_smarty_tpl->tpl_vars['i']->value;?>
</a>
                        <?php }
}
?>
                    </p>
                </span>
            <?php } else { ?>
                <div class="search__false">
                    <h3>
                        Ничего не найдено(
                    </h3>
                    <p>
                        <img src="views/img/searchNothing.png">
                    </p>
                </div>
            <?php }?>
        </div>
    </div>
</div>
<?php }
}
