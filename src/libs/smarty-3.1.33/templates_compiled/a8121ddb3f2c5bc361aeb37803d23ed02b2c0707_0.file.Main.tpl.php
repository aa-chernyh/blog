<?php
/* Smarty version 3.1.33, created on 2019-05-30 21:47:04
  from 'W:\domains\blog\views\templates\Main.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cf025284d5154_35719820',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a8121ddb3f2c5bc361aeb37803d23ed02b2c0707' => 
    array (
      0 => 'W:\\domains\\blog\\views\\templates\\Main.tpl',
      1 => 1559241974,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cf025284d5154_35719820 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="shortcut icon" href="/src/viewsiews/img/favicon.png" type="image/png">
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
 | Блог высоких технологий</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="views/css/Main.css">
        <?php echo '<script'; ?>
 src="http://code.jquery.com/jquery-1.8.3.js"><?php echo '</script'; ?>
>
        <?php echo '<script'; ?>
 src="views/js/Main.js"><?php echo '</script'; ?>
>
        <?php if (isset($_smarty_tpl->tpl_vars['stylesPath']->value)) {?>
            <link rel="stylesheet" href="<?php echo $_smarty_tpl->tpl_vars['stylesPath']->value;?>
">
        <?php }?>
        <?php if (isset($_smarty_tpl->tpl_vars['scriptsPath']->value)) {?>
            <?php echo '<script'; ?>
 src="<?php echo $_smarty_tpl->tpl_vars['scriptsPath']->value;?>
"><?php echo '</script'; ?>
>
        <?php }?>
    </head>
    <body>
        <div class="header">
            <div class="header__wrapper fixed">
                <div class="headerPanel flex">
                    <div class="headerPanel__logo">
                        <a href="<?php if ($_smarty_tpl->tpl_vars['isLoginPage']->value) {?>login<?php } else { ?>enter<?php }?>">
                            <i class="fas fa-desktop fa-2x"></i>
                        </a>
                    </div>
                    <div class="headerPanel__searchForm">
                        <form action="search" class="flex" id="search">
                            <input type="text" name="searchText" class="headerPanel__searchForm__input" placeholder="Поиск" value="<?php echo $_smarty_tpl->tpl_vars['searchText']->value;?>
">
                            <i class="headerPanel__searchForm__startIcon fas fa-search" onclick="doSearch('<?php echo $_smarty_tpl->tpl_vars['isLoginPage']->value;?>
')"></i>
                        </form>
                    </div>
                    <div class="headerPanel__user flex">
                        <?php if ($_smarty_tpl->tpl_vars['isLoginPage']->value) {?>
                            <div class="headerPanel__user__requireEnter">
                                <p>Необходимо войти</p>
                            </div>
                        <?php } else { ?>
                            <div class="headerPanel__user__name">
                                <a href="profile"><?php echo $_smarty_tpl->tpl_vars['User']->value['Name'];?>
</a>
                            </div>
                            <div class="headerPanel__user__avatar">
                                <?php if ($_smarty_tpl->tpl_vars['Avatar']->value) {?>
                                    
                                <?php } else { ?>
                                    <img src="views/img/defaultAvatar.png" alt="avatar">
                                <?php }?>
                            </div>
                            <div class="headerPanel__user__logout">
                                <a href="login?action=logout">Выйти</a>
                            </div>
                        <?php }?>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <?php $_smarty_tpl->_subTemplateRender(((string)$_smarty_tpl->tpl_vars['template']->value), $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
        </div>
        <div class="footer">
            <div class="footer__wrapper">
                <div class="footerPanel flex">
                    <div class="footerPanel__siteDescription">
                        <p>Блог высоких технологий © 2014-<?php echo $_smarty_tpl->tpl_vars['currentYear']->value;?>
</p>
                    </div>
                    <div class="footerPanel__socialNetworkLinks">
                        <a href="https://vk.com/" target="_blank">
                            <i class="fab fa-vk fa-2x"></i>
                        </a>
                        <a href="https://ru-ru.facebook.com/" target="_blank">
                            <i class="fab fa-facebook fa-2x"></i>
                        </a>
                        <a href="https://twitter.com/?lang=ru" target="_blank">
                            <i class="fab fa-twitter fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html><?php }
}
