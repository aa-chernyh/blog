<?php
/* Smarty version 3.1.33, created on 2019-05-20 20:49:45
  from 'W:\domains\blog\views\templates\main.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5ce2e8b97b3182_24499567',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'b895ed8e4f67d784a059f848fcefddf5b53296ea' => 
    array (
      0 => 'W:\\domains\\blog\\views\\templates\\main.tpl',
      1 => 1558374547,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:views/templates/".((string)$_smarty_tpl->tpl_vars[\'file\']->value).".tpl' => 1,
  ),
),false)) {
function content_5ce2e8b97b3182_24499567 (Smarty_Internal_Template $_smarty_tpl) {
?><!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>title | Блог высоких технологий</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="views/css/mainTemplate.css">
    </head>
    <body>
        <div class="header">
            <div class="header__wrapper fixed">
                <div class="headerPanel flex">
                    <div class="headerPanel__logo">
                        <a href="index.html">
                            <i class="fas fa-desktop fa-2x"></i>
                        </a>
                    </div>
                    <div class="headerPanel__searchForm">
                        <form action="" class="flex">
                            <input type="text" name="searchText" class="headerPanel__searchForm__input" placeholder="Поиск">
                            <i class="headerPanel__searchForm__startIcon fas fa-search" onclick="submit();"></i>
                        </form>
                    </div>
                    <div class="headerPanel__user flex">
                        <div class="headerPanel__user__name bold">
                            <a href="profile.html">Александр</a>
                        </div>
                        <div class="headerPanel__user__avatar">
                            <img src="views/img/flag.jpg" alt="avatar">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            <?php $_smarty_tpl->_subTemplateRender("file:views/templates/".((string)$_smarty_tpl->tpl_vars['file']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, true);
?>
        </div>
        <div class="footer">
            <div class="footer__wrapper">
                <div class="footerPanel flex">
                    <div class="footerPanel__siteDescription">
                        <p>Блог высоких технологий © 2014-currentYear</p>
                    </div>
                    <div class="footerPanel__socialNetworkLinks">
                        <a href="https://vk.com/" target="_blank">
                            <i class="fab fa-vk fa-2x"></i>
                        </a>
                        <a href="https://ru-ru.facebook.com/" target="_blank">
                            <i class="fab fa-facebook fa-2x"></i>
                        </a>
                        <a href="https://twitter.com/?lang=ru" target="_blank">
                            <i class="fab fa-twitter fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html><?php }
}
