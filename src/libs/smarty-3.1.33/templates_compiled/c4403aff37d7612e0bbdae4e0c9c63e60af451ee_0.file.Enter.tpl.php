<?php
/* Smarty version 3.1.33, created on 2019-05-30 01:13:34
  from 'W:\domains\blog\views\templates\Enter.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cef040ed11961_79773182',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c4403aff37d7612e0bbdae4e0c9c63e60af451ee' => 
    array (
      0 => 'W:\\domains\\blog\\views\\templates\\Enter.tpl',
      1 => 1559168013,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cef040ed11961_79773182 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="content__wrapper">
    <div class="background">
        <?php
$__section_sect_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['Data']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_sect_0_total = $__section_sect_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_sect'] = new Smarty_Variable(array());
if ($__section_sect_0_total !== 0) {
for ($__section_sect_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index'] = 0; $__section_sect_0_iteration <= $__section_sect_0_total; $__section_sect_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index']++){
?>
            <div class="section" id="section_<?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index'] : null)]['Id'];?>
">
                <div class="section__header flex">
                    <div class="section__img"></div>
                    <div class="section__text">
                        <?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index'] : null)]['Name'];?>

                    </div>
                    <div class="section__messageCount">
                        Сообщений
                    </div>
                    <div class="section__toggleSection">
                        <img src="/src/viewsiews/img/hide.png" alt="toggle" onclick="toggleSection(<?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index'] : null)]['Id'];?>
);" data-position="show">
                    </div>
                </div>
                <div class="section__toggle">
                    <?php
$__section_theme_1_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index'] : null)]['Themes']) ? count($_loop) : max(0, (int) $_loop));
$__section_theme_1_total = $__section_theme_1_loop;
$_smarty_tpl->tpl_vars['__smarty_section_theme'] = new Smarty_Variable(array());
if ($__section_theme_1_total !== 0) {
for ($__section_theme_1_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index'] = 0; $__section_theme_1_iteration <= $__section_theme_1_total; $__section_theme_1_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index']++){
?>
                        <div class="section__theme flex">
                            <div class="section__theme__img">
                                <img src="/src/viewsiews/img/theme.png" alt="theme message" class="toggleImg" <?php if ($_smarty_tpl->tpl_vars['User']->value['Privilege'] == "Администратор") {?>onmouseover="toggleDeleteIcon(this);"<?php }?>>
                                <img src="views/img/delete.png" alt="delete" class="hide toggleImg pointer" <?php if ($_smarty_tpl->tpl_vars['User']->value['Privilege'] == "Администратор") {?>onmouseleave="toggleDeleteIcon(this)" onclick="alertAndSubmit(this);"<?php }?>>
                                <?php if ($_smarty_tpl->tpl_vars['User']->value['Privilege'] == "Администратор") {?>
                                    <form action="enter" method="POST">
                                        <input type="hidden" name="action" value="deleteTheme">
                                        <input type="hidden" name="userId" value="<?php echo $_smarty_tpl->tpl_vars['User']->value['Id'];?>
">
                                        <input type="hidden" name="themeId" value="<?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index'] : null)]['Themes'][(isset($_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index'] : null)]['Id'];?>
">
                                    </form>
                                <?php }?>
                            </div>
                            <div class="section__text">
                                <div class="section__text__themeName bold">
                                    <a href="theme?id=<?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index'] : null)]['Themes'][(isset($_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index'] : null)]['Id'];?>
"><?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index'] : null)]['Themes'][(isset($_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index'] : null)]['Name'];?>
</a>
                                </div>
                                <div class="section__text__themeFirstMessage">
                                    <?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index'] : null)]['Themes'][(isset($_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index'] : null)]['LastMessage'];?>

                                </div>
                            </div>
                            <div class="section__messageCount">
                                <?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index'] : null)]['Themes'][(isset($_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_theme']->value['index'] : null)]['MessagesCount'];?>

                            </div>
                            <div class="section__toggleSection"></div>
                        </div>
                    <?php
}
}
?>
                    <div class="section__addTheme flex">
                        <div class="section__addTheme__img">
                            <img src="/src/viewsiews/img/add.png" alt="theme message">
                        </div>
                        <div class="section__text">
                            <form action="enter" method="POST" id="createTheme">
                                <a href="javascript:void(0);" onclick="create(this);">Добавить тему</a>
                                <input type="text" name="name" placeholder="название">
                                <input type="hidden" name="action" value="createTheme">
                                <input type="hidden" name="sectionId" value="<?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_sect']->value['index'] : null)]['Id'];?>
">
                                <input type="hidden" name="userId" value="<?php echo $_smarty_tpl->tpl_vars['User']->value['Id'];?>
">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        <?php
}
}
?>
        <?php if ($_smarty_tpl->tpl_vars['User']->value['Privilege'] == "Администратор") {?>
            <div class="addSection flex">
                <div class="section__addTheme__img">
                    <img src="/src/viewsiews/img/add.png" alt="theme message">
                </div>
                <div class="section__text">
                    <form action="enter" method="POST" id="createSection">
                        <a href="javascript:void(0);" onclick="create(this);">Создать раздел</a>
                        <input type="text" name="name" placeholder="название">
                        <input type="hidden" name="userId" value="<?php echo $_smarty_tpl->tpl_vars['User']->value['Id'];?>
">
                        <input type="hidden" name="action" value="createSection">
                    </form>
                </div>
            </div>
        <?php }?>
    </div>
</div>
<?php }
}
