<?php
/* Smarty version 3.1.33, created on 2019-05-30 01:35:59
  from 'W:\domains\blog\views\templates\Theme.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5cef094f7d5621_44095034',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'd5eab827fae6e855b61ca22baf9c92a8d0b6c405' => 
    array (
      0 => 'W:\\domains\\blog\\views\\templates\\Theme.tpl',
      1 => 1559169357,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5cef094f7d5621_44095034 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="content__wrapper">
    <div class="background">
        <div class="theme">
            <div class="theme__name">
                <p id="pageHead"><span class="bold">Тема: </span><?php echo $_smarty_tpl->tpl_vars['ThemeName']->value;?>
</p>
            </div>
            <?php
$__section_message_0_loop = (is_array(@$_loop=$_smarty_tpl->tpl_vars['Data']->value) ? count($_loop) : max(0, (int) $_loop));
$__section_message_0_total = $__section_message_0_loop;
$_smarty_tpl->tpl_vars['__smarty_section_message'] = new Smarty_Variable(array());
if ($__section_message_0_total !== 0) {
for ($__section_message_0_iteration = 1, $_smarty_tpl->tpl_vars['__smarty_section_message']->value['index'] = 0; $__section_message_0_iteration <= $__section_message_0_total; $__section_message_0_iteration++, $_smarty_tpl->tpl_vars['__smarty_section_message']->value['index']++){
?>
                <div class="theme__message">
                    <div class="theme__message__header flex">
                        <img src="/src/viewsiews/img/whiteList.png" alt="message">
                        <span class="theme__message__header__date">
                            <?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_message']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_message']->value['index'] : null)]['CreationDate'];?>

                        </span>
                        <?php if ($_smarty_tpl->tpl_vars['User']->value['Privilege'] == "Администратор") {?>
                            <span class="theme__message__remove">
                                <img src="/src/viewsiews/img/delete.png" alt="delete" onclick="alertAndSubmit(this)" class="pointer">
                                <form action="theme?id=<?php echo $_smarty_tpl->tpl_vars['ThemeId']->value;?>
" method="POST">
                                    <input type="hidden" name="userId" value="<?php echo $_smarty_tpl->tpl_vars['User']->value['Id'];?>
">
                                    <input type="hidden" name="messageId" value="<?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_message']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_message']->value['index'] : null)]['Id'];?>
">
                                    <input type="hidden" name="action" value="deleteMessage">
                                </form>
                            </span>
                        <?php }?>
                    </div>
                    <div class="theme__message__content flex">
                        <div class="theme__message__content__userProfile">
                            <div class="theme__message__content__userProfile__nickname">
                                <?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_message']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_message']->value['index'] : null)]['UserName'];?>

                            </div>
                            <div class="theme__message__content__userProfile__avatar">
                                <img src="views/img/defaultAvatar.png" alt="user avatar">
                            </div>
                        </div>
                        <div class="theme__message__content__body">
                            <div class="theme__message__content__body__text">
                                <?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_message']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_message']->value['index'] : null)]['Text'];?>

                            </div>
                            <hr>
                            <div class="theme__message__content__body__userSign">
                                <?php echo $_smarty_tpl->tpl_vars['Data']->value[(isset($_smarty_tpl->tpl_vars['__smarty_section_message']->value['index']) ? $_smarty_tpl->tpl_vars['__smarty_section_message']->value['index'] : null)]['UserSign'];?>

                            </div>
                        </div>
                    </div>
                    <div class="theme__message__footer float">
                        <a href="#pageHead">
                            Вверх <img src="/src/viewsiews/img/arrowUp.png" alt="arrow up">
                        </a>
                    </div>
                </div>
            <?php
}
}
?>
            <!--Написать сообщение-->
            <div class="theme__addMessage">
                <div class="theme__addMessage__header flex">
                    <img src="/src/viewsiews/img/whiteList.png" alt="message">
                    <span class="theme__addMessage__header__title">Написать сообщение:</span>
                </div>
                <div class="theme__addMessage__body">
                    <form action="theme?id=<?php echo $_smarty_tpl->tpl_vars['ThemeId']->value;?>
" method="POST">
                        <textarea name="text" id="theme__addMessage__body__textarea"></textarea>
                        <input type="hidden" name="userId" value="<?php echo $_smarty_tpl->tpl_vars['User']->value['Id'];?>
">
                        <input type="hidden" name="themeId" value="<?php echo $_smarty_tpl->tpl_vars['ThemeId']->value;?>
">
                        <input type="hidden" name="action" value="addMessage">
                        <div>
                            <button type="button" class="theme__addMessage__body__addMessage button" onclick="checkTextareaAndSubmit(this)">Добавить комментарий</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
<?php }
}
