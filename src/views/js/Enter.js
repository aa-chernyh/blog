function toggleSection(sectionNumber) {
    $("#section_" +sectionNumber + " .section__toggle").slideToggle("slow");
    let toggleImg = $("#section_" +sectionNumber + " .section__toggleSection img").prop("data-position") == "show" ? "hide" : "show";
    $("#section_" +sectionNumber + " .section__toggleSection img").prop("src", "/views/img/" + toggleImg + ".png");
    $("#section_" +sectionNumber + " .section__toggleSection img").prop("data-position", toggleImg);
}

function createSection() {
    let input = $('#createSection > input[name=name]');
    if(input.val() !== ''){
        input.removeClass('error');
        $('#createSection').submit();
    } else {
        input.addClass('error');
    }
}

function createTheme(section) {
    let input = $('#section_' + section + ' #createTheme > input[name=name]');
    if(input.val() !== ''){
        input.removeClass('error');
        $('#createTheme').submit();
    } else {
        input.addClass('error');
    }
}

function create(elem) {
    let input = $(elem).parent().children('input[name=name]');
    let form = $(elem).parent();
    if(input.val() !== ''){
        input.removeClass('error');
        form.submit();
    } else {
        input.addClass('error');
    }
}

function toggleDeleteIcon(elem) {
    $(elem).parent().children(".toggleImg").each(function(){
        $(this).toggle();
    });
}