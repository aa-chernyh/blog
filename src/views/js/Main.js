function alertAndSubmit(elem) {
    let form = $(elem).parent().children("form")[0];
    if(confirm("Удалить?")){
        $(form).submit();
    }
}

function checkFieldsAndSubmit(button) {
    let inputs = $(button).parent().parent().children('input');
    inputs.each(function(){
        $(this).removeClass('error');
    });
    let errors = false;
    inputs.each(function(){
        if($(this).val() === '') {
            $(this).addClass('error');
            errors = true;
        }
    });
    
    if(!errors) {
        $(button).parent().parent().submit();
    }
}

function doSearch(isLoginPage) {
    let searchInput = $('.headerPanel__searchForm__input');
    searchInput.removeClass('error');
    if(isLoginPage !== '') {
        $('.headerPanel__user__requireEnter p').addClass('colorRed');
    } else if(searchInput.val() !== '') {
        $('#search').submit();
    } else {
       searchInput.addClass('error');
    }
}

function checkTextareaAndSubmit(button) {
    let textarea = $(button).parent().parent().children('textarea')[0];
    $(textarea).removeClass('error');
    if($(textarea).val() !== '') {
        $(textarea).parent().submit();
    } else {
        $(textarea).addClass('error');
    }
}
