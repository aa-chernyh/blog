<div class="content__wrapper">
    <div class="background">
        {section name=sect loop=$Data}
            <div class="section" id="section_{$Data[sect].Id}">
                <div class="section__header flex">
                    <div class="section__img"></div>
                    <div class="section__text">
                        {$Data[sect].Name}
                    </div>
                    <div class="section__messageCount">
                        Сообщений
                    </div>
                    <div class="section__toggleSection">
                        <img src="/src/viewsiews/img/hide.png" alt="toggle" onclick="toggleSection({$Data[sect].Id});" data-position="show">
                    </div>
                </div>
                <div class="section__toggle">
                    {section name=theme loop=$Data[sect].Themes}
                        <div class="section__theme flex">
                            <div class="section__theme__img">
                                <img src="/src/viewsiews/img/theme.png" alt="theme message" class="toggleImg" {if $User.Privilege == "Администратор"}onmouseover="toggleDeleteIcon(this);"{/if}>
                                <img src="views/img/delete.png" alt="delete" class="hide toggleImg pointer" {if $User.Privilege == "Администратор"}onmouseleave="toggleDeleteIcon(this)" onclick="alertAndSubmit(this);"{/if}>
                                {if $User.Privilege == "Администратор"}
                                    <form action="enter" method="POST">
                                        <input type="hidden" name="action" value="deleteTheme">
                                        <input type="hidden" name="userId" value="{$User.Id}">
                                        <input type="hidden" name="themeId" value="{$Data[sect].Themes[theme].Id}">
                                    </form>
                                {/if}
                            </div>
                            <div class="section__text">
                                <div class="section__text__themeName bold">
                                    <a href="theme?id={$Data[sect].Themes[theme].Id}">{$Data[sect].Themes[theme].Name}</a>
                                </div>
                                <div class="section__text__themeFirstMessage">
                                    {$Data[sect].Themes[theme].LastMessage}
                                </div>
                            </div>
                            <div class="section__messageCount">
                                {$Data[sect].Themes[theme].MessagesCount}
                            </div>
                            <div class="section__toggleSection"></div>
                        </div>
                    {/section}
                    <div class="section__addTheme flex">
                        <div class="section__addTheme__img">
                            <img src="/src/viewsiews/img/add.png" alt="theme message">
                        </div>
                        <div class="section__text">
                            <form action="enter" method="POST" id="createTheme">
                                <a href="javascript:void(0);" onclick="create(this);">Добавить тему</a>
                                <input type="text" name="name" placeholder="название">
                                <input type="hidden" name="action" value="createTheme">
                                <input type="hidden" name="sectionId" value="{$Data[sect].Id}">
                                <input type="hidden" name="userId" value="{$User.Id}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        {/section}
        {if $User.Privilege == "Администратор"}
            <div class="addSection flex">
                <div class="section__addTheme__img">
                    <img src="/src/viewsiews/img/add.png" alt="theme message">
                </div>
                <div class="section__text">
                    <form action="enter" method="POST" id="createSection">
                        <a href="javascript:void(0);" onclick="create(this);">Создать раздел</a>
                        <input type="text" name="name" placeholder="название">
                        <input type="hidden" name="userId" value="{$User.Id}">
                        <input type="hidden" name="action" value="createSection">
                    </form>
                </div>
            </div>
        {/if}
    </div>
</div>
