<div class="content__wrapper">
    <div class="loginPage">
        <div class="loginPage__login">
            <div class="loginPage__register__description">
                <h2>Вход</h2>
                <p class="loginPage__register__description__error red {if $isLoginSuccess}hide{/if}">{$LoginErrorText}</p>
            </div>
            <form action="login{$get}" method="POST">
                <input type="text" name="email" placeholder="E-mail" class="loginPage__input">
                <input type="password" name="password" placeholder="Пароль" class="loginPage__input">
                <input type="hidden" name="action" value="login">
                <div>
                    <button type="button" class="button loginPage__login__submit" onclick="checkFieldsAndSubmit(this)">Войти</button>
                </div>
            </form>
        </div>
        <div class="loginPage__register">
            <div class="loginPage__register__description">
                <h2>Впервые здесь?</h2>
                <p class="{if $isRegisterFail}hide{/if}">Моментальная регистрация</p>
                <p class="red {if !$isRegisterFail}hide{/if}">{$RegisterErrorText}</p>
            </div>
            <form action="login{$get}" method="POST">
                <input type="text" name="name" placeholder="Ваше имя" class="loginPage__input">
                <input type="text" name="email" placeholder="E-mail" class="loginPage__input">
                <input type="password" name="password" placeholder="Пароль" class="loginPage__input">
                <input type="password" name="passwordConfirm" placeholder="Подтвердите пароль" class="loginPage__input">
                <input type="hidden" name="action" value="registration">
                <div>
                    <button type="button" class="button loginPage__register__submit" onclick="checkFieldsAndSubmit(this)">Закончить регистрацию</button>
                </div>
            </form>
        </div>
    </div>
</div>