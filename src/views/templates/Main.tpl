<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <link rel="shortcut icon" href="/src/viewsiews/img/favicon.png" type="image/png">
        <title>{$title} | Блог высоких технологий</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
        <link rel="stylesheet" href="views/css/Main.css">
        <script src="http://code.jquery.com/jquery-1.8.3.js"></script>
        <script src="views/js/Main.js"></script>
        {if isset($stylesPath)}
            <link rel="stylesheet" href="{$stylesPath}">
        {/if}
        {if isset($scriptsPath)}
            <script src="{$scriptsPath}"></script>
        {/if}
    </head>
    <body>
        <div class="header">
            <div class="header__wrapper fixed">
                <div class="headerPanel flex">
                    <div class="headerPanel__logo">
                        <a href="{if $isLoginPage}login{else}enter{/if}">
                            <i class="fas fa-desktop fa-2x"></i>
                        </a>
                    </div>
                    <div class="headerPanel__searchForm">
                        <form action="search" class="flex" id="search">
                            <input type="text" name="searchText" class="headerPanel__searchForm__input" placeholder="Поиск" value="{$searchText}">
                            <i class="headerPanel__searchForm__startIcon fas fa-search" onclick="doSearch('{$isLoginPage}')"></i>
                        </form>
                    </div>
                    <div class="headerPanel__user flex">
                        {if $isLoginPage}
                            <div class="headerPanel__user__requireEnter">
                                <p>Необходимо войти</p>
                            </div>
                        {else}
                            <div class="headerPanel__user__name">
                                <a href="profile">{$User.Name}</a>
                            </div>
                            <div class="headerPanel__user__avatar">
                                {if $Avatar}
                                    
                                {else}
                                    <img src="views/img/defaultAvatar.png" alt="avatar">
                                {/if}
                            </div>
                            <div class="headerPanel__user__logout">
                                <a href="login?action=logout">Выйти</a>
                            </div>
                        {/if}
                    </div>
                </div>
            </div>
        </div>
        <div class="content">
            {include file="{$template}"}
        </div>
        <div class="footer">
            <div class="footer__wrapper">
                <div class="footerPanel flex">
                    <div class="footerPanel__siteDescription">
                        <p>Блог высоких технологий © 2014-{$currentYear}</p>
                    </div>
                    <div class="footerPanel__socialNetworkLinks">
                        <a href="https://vk.com/" target="_blank">
                            <i class="fab fa-vk fa-2x"></i>
                        </a>
                        <a href="https://ru-ru.facebook.com/" target="_blank">
                            <i class="fab fa-facebook fa-2x"></i>
                        </a>
                        <a href="https://twitter.com/?lang=ru" target="_blank">
                            <i class="fab fa-twitter fa-2x"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>