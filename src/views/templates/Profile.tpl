<div class="content__wrapper">
    <div class="background">
        <div class="profile">
            <div class="profile__title">
                <h2>Профиль пользователя {$User.Name}</h2>
            </div>
            <div class="profile__section flex">
                <div class="profile__section__avatar">
                    <img src="/src/viewsiews/img/defaultAvatar.png" alt="avatar" onmouseover="$('.profile__section__avatar__change').slideDown('');">
                    <div class="profile__section__avatar__change hide">
                        <form action="profile" enctype="multipart/form-data" method="POST">
                            <p class="bold pointer">
                                <input type="hidden" name="action" value="changeAvatar">
                                <input type="hidden" name="userId" value="{$User.Id}">
                                <input type="file" value="Изменить аватарку" onchange="$('.profile__section__avatar__change form').submit();">
                            </p>
                        </form>
                    </div>
                </div>
                <div class="profile__section__password">
                    <p class="profile__section__password__title {if isset($ChangePasswordResult)}hide{/if}">Сменить пароль:</p>
                    <p class="profile__section__password__title green {if !$ChangePasswordResult}hide{/if}">Пароль успешно изменен!</p>
                    <p class="profile__section__password__title red {if $ChangePasswordResult}hide{/if}">{$ChangePasswordErrorText}</p>
                    <form action="profile" method="POST">
                        <input type="password" name="oldPassword" class="input" placeholder="Старый пароль" value="{$OldPassword}">
                        <input type="password" name="newPassword" class="input" placeholder="Новый пароль" value="{$NewPassword}">
                        <input type="password" name="newPasswordConfirm" class="input" placeholder="Новый пароль ещё разок" value="{$NewPasswordConfirm}">
                        <input type="hidden" name="userId" value="{$User.Id}">
                        <input type="hidden" name="action" value="changePassword">
                        <div>
                            <button type="button" class="button profile__section__password__confirm" onclick="checkFieldsAndSubmit(this);">Подтвердить</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="profile__section">
                <table class="profile__section__userInfo">
                    <tr>
                        <td>Email</td>
                        <td>{$User.Email}</td>
                    </tr>
                    <tr>
                        <td>Дата регистрации</td>
                        <td>{$User.RegDate}</td>
                    </tr>
                    <tr>
                        <td>Привилегии</td>
                        <td>{$User.Privilege}</td>
                    </tr>
                    <tr>
                        <td>Сообщений написано</td>
                        <td>{$User.MessagesCount}</td>
                    </tr>
                    <tr>
                        <td>Тем создано</td>
                        <td>{$User.ThemesCount}</td>
                    </tr>
                </table>
            </div>
            <div class="profile__section">
                <div class="profile__section__userSign">
                    <p class="profile__section__userSign__title {if isset($ChangeSignResult)}hide{/if}">Подпись под сообщениями:</p>
                    <p class="profile__section__userSign__title green {if !$ChangeSignResult}hide{/if}">Подпись успешно изменена!</p>
                    <p class="profile__section__userSign__title red {if $ChangeSignResult}hide{/if}">{$ChangeSignErrorText}</p>
                    <div class="profile__section__userSign__change">
                        <form action="profile" method="POST">
                            <textarea class="input" name="sign">{$User.Sign}</textarea>
                            <input type="hidden" name="userId" value="{$User.Id}">
                            <input type="hidden" name="action" value="changeSign">
                            <div>
                                <button type="button" class="button profile__section__userSign__change__button" onclick="checkTextareaAndSubmit(this);">Подтвердить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
