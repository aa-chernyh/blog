<div class="content__wrapper">
    <div class="background">
        <div class="search">
            {if !empty($Themes) || !empty($Messages)}
                {if !empty($Themes)}
                    <div class="search__section">
                        <div class="search__section__title">
                            <h2>Темы:</h2>
                        </div>
                        {section name=theme loop=$Themes}
                            <div class="search__section__results">
                                <div class="search__section__results__text">
                                    <a href="theme?id={$Themes[theme].Id}" target="_blank">
                                        {$Themes[theme].Name}
                                    </a>
                                </div>
                            </div>
                        {/section}
                    </div>
                {/if}
                {if !empty($Messages)}
                    <div class="search__section">
                        <div class="search__section__title">
                            <h2>Сообщения:</h2>
                        </div>
                        <div class="search__section__results">
                            {section name=message loop=$Messages}
                                <div class="search__section__results__text">
                                    <a href="theme?id={$Messages[message].ThemeId}" target="_blank">
                                        {$Messages[message].Text}
                                    </a>
                                </div>
                            {/section}
                        </div>
                    </div>
                {/if}
                <span class="search__pagination">
                    <p>
                        {for $i=1 to $countPages}
                            <a href="search?searchText={$searchText}&page={$i}">{$i}</a>
                        {/for}
                    </p>
                </span>
            {else}
                <div class="search__false">
                    <h3>
                        Ничего не найдено(
                    </h3>
                    <p>
                        <img src="views/img/searchNothing.png">
                    </p>
                </div>
            {/if}
        </div>
    </div>
</div>
