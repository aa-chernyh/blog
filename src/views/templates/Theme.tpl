<div class="content__wrapper">
    <div class="background">
        <div class="theme">
            <div class="theme__name">
                <p id="pageHead"><span class="bold">Тема: </span>{$ThemeName}</p>
            </div>
            {section name=message loop=$Data}
                <div class="theme__message">
                    <div class="theme__message__header flex">
                        <img src="/src/viewsiews/img/whiteList.png" alt="message">
                        <span class="theme__message__header__date">
                            {$Data[message].CreationDate}
                        </span>
                        {if $User.Privilege == "Администратор"}
                            <span class="theme__message__remove">
                                <img src="/src/viewsiews/img/delete.png" alt="delete" onclick="alertAndSubmit(this)" class="pointer">
                                <form action="theme?id={$ThemeId}" method="POST">
                                    <input type="hidden" name="userId" value="{$User.Id}">
                                    <input type="hidden" name="messageId" value="{$Data[message].Id}">
                                    <input type="hidden" name="action" value="deleteMessage">
                                </form>
                            </span>
                        {/if}
                    </div>
                    <div class="theme__message__content flex">
                        <div class="theme__message__content__userProfile">
                            <div class="theme__message__content__userProfile__nickname">
                                {$Data[message].UserName}
                            </div>
                            <div class="theme__message__content__userProfile__avatar">
                                <img src="views/img/defaultAvatar.png" alt="user avatar">
                            </div>
                        </div>
                        <div class="theme__message__content__body">
                            <div class="theme__message__content__body__text">
                                {$Data[message].Text}
                            </div>
                            <hr>
                            <div class="theme__message__content__body__userSign">
                                {$Data[message].UserSign}
                            </div>
                        </div>
                    </div>
                    <div class="theme__message__footer float">
                        <a href="#pageHead">
                            Вверх <img src="/src/viewsiews/img/arrowUp.png" alt="arrow up">
                        </a>
                    </div>
                </div>
            {/section}
            <!--Написать сообщение-->
            <div class="theme__addMessage">
                <div class="theme__addMessage__header flex">
                    <img src="/src/viewsiews/img/whiteList.png" alt="message">
                    <span class="theme__addMessage__header__title">Написать сообщение:</span>
                </div>
                <div class="theme__addMessage__body">
                    <form action="theme?id={$ThemeId}" method="POST">
                        <textarea name="text" id="theme__addMessage__body__textarea"></textarea>
                        <input type="hidden" name="userId" value="{$User.Id}">
                        <input type="hidden" name="themeId" value="{$ThemeId}">
                        <input type="hidden" name="action" value="addMessage">
                        <div>
                            <button type="button" class="theme__addMessage__body__addMessage button" onclick="checkTextareaAndSubmit(this)">Добавить комментарий</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
